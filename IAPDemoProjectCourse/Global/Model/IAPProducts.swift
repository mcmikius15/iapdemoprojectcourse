//
//  IAPProducts.swift
//  IAPDemoProjectCourse
//
//  Created by Mykhailo Bondarenko on 30/10/2017.
//  Copyright © 2017 Mykhailo Bondarenko. All rights reserved.
//

import Foundation

enum IAPProducts: String {
    case consumable = "com.mcmikius.consumable"
    case nonConsumable = "com.mcmikius.nonConsumable"
    case autoRenewable = "com.mcmikius.autoRenewable"
    case nonRenewable = "com.mcmikius.nonRenewing"
}
