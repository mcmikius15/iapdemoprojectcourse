//
//  CellIdentifiers.swift
//  IAPDemoProject
//
//  Created by Mykhailo Bondarenko on 26/10/2017.
//  Copyright © 2017 Mykhailo Bondarenko. All rights reserved.
//

import Foundation

enum CellIdentifiers {
    static let mainCell = "mainCell"
    static let purchaseCell = "purchaseCell"
}
